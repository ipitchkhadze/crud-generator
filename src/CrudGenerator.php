<?php

namespace Tks\CrudGenerator;

use Tks\CrudGenerator\Services\CrudGeneratorService;

class CrudGenerator
{
    private CrudGeneratorService $crudGeneratorService;
    public function __construct()
    {
        $this->crudGeneratorService = new CrudGeneratorService();
    }

    public function generateFromJson(string $jsonFilePath)
    {
        $this->crudGeneratorService->generate($jsonFilePath);
    }

    public function test()
    {
        echo 'Hello World!!!';
    }
}
