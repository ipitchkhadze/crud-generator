<?php

namespace Tks\CrudGenerator\Services;

use Carbon\Carbon;
use Illuminate\Console\Concerns\InteractsWithIO;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Tks\CrudGenerator\Models\Entity;

class CrudGeneratorService
{
    use InteractsWithIO;

    /**
     * @var mixed
     */
    private array $entity;
    private Filesystem $files;
    private bool $withEntityData;
    protected Application $laravel;

    public function __construct($withEntityData = false)
    {
        $this->laravel = \app();
        $this->entity = [];
        $this->files = new Filesystem();
        $this->withEntityData = $withEntityData;
    }

    public function generate(string $jsonFilePath)
    {
        $data = file_get_contents($jsonFilePath);
        $this->entity = json_decode($data, true);

        $this->saveData();
        $this->makeController();
        $this->makeModel();
        $this->makeResource();
        $this->makeCollection();
        $this->makeMigration();
        $this->makeRequest();
        $this->makeSeeder();
        $this->addRoute();
        if(App::runningInConsole()) {
            $this->info('Entity created successfully!');
        }
        return true;
    }

    protected function addAttribute()
    {
        $attribute = [];

        $attribute['name'] = $this->ask('Attribute name');
        $data = explode('|', $this->ask('Attribute code example (name:string|nullable:unique)'));

        list($attribute['code'], $attribute['type']) = explode(':', $data[0]);
        if (isset($data[1])) {
            $attributeInfo = explode(':', $data[1]);
            $attribute['nullable'] = (in_array('nullable', $attributeInfo)) ? true : false;
            $attribute['unique'] = (in_array('unique', $attributeInfo)) ? true : false;
        }
        $this->entity['attributes'][] = $attribute;
    }

    protected function saveData()
    {
        if(!$this->withEntityData){
            return true;
        }
        try {
            $this->saveToEntitySeeder();
            $this->saveToEntityAttributeSeeder();

            $entity = Entity::updateOrCreate([
                'code' => $this->entity['code'],
            ], [
                'name' => $this->entity['name'],
                'main_attribute' => $this->entity['main_attribute'],
            ]);

            foreach ($this->entity['attributes'] as $attribute) {
                $entity->attributes()->create($attribute);
            }
            return true;
        } catch (Exception $e) {
            dump($this->entity);
            if(App::runningInConsole()) {
                $this->error($e->getMessage());
                $this->error('Failed write to database!');
            }
            die;
        }

    }

    protected function makeMigration()
    {

        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $stub = $stub = $this->files->get($this->getStub('migration'));
        $fields = [];
        $relations = [];

        $replaces = [
            '{{table}}' => Str::plural($name, 2),
            '{{utable}}' => Str::plural($uname, 2),
        ];

        foreach ($this->entity['attributes'] as $attribute) {
            $nullable = (isset($attribute['nullable']) && $attribute['nullable'] == true) ? "->nullable()" : '';
            $unique = (isset($attribute['unique']) && $attribute['unique'] == true) ? "->unique()" : '';
            $fields[] = '$table->'.$attribute['type']."('".strtolower($attribute['code'])."')".$nullable.$unique.";";
            if (isset($attribute['relation'])) {
                $relations[] = '$table->foreign(\''.strtolower($attribute['code']).'\')->references(\'id\')->on(\''.$attribute['relation']['table'].'\');';
            }
        }

        $replaces['{{fields}}'] = implode("\r\n\t\t\t", $fields);
        $replaces['{{relations}}'] = implode("\r\n\t\t\t", $relations);

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }
        $path = base_path('database/migrations/').Carbon::now()->format('Y_m_d_His').'_create_'.Str::plural($name,
                2).'_table.php';

        $this->files->put($path, $stub);

    }

    protected function makeSeeder()
    {

        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $pluralName = Str::plural($name, 2);
        $stub = $stub = $this->files->get($this->getStub('seeder'));
        $fields = [];

        $replaces = [
            '{{name}}' => $name,
            '{{uname}}' => $uname,
            '{{plural_name}}' => $pluralName,
            '{{main_attribute}}' => $this->entity['main_attribute'],
        ];

        foreach ($this->entity['attributes'] as $attribute) {
            $fields[] = "'".$attribute['code']."' => ".$this->getSeederAttributeValue($attribute['type']);
        }

        $replaces['{{fields}}'] = implode(",\r\n\t\t\t", $fields);

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }

        $path = base_path('database/seeders/').$uname.'Seeder.php';

        $this->files->put($path, $stub);

        $path = base_path('database/seeders/DatabaseSeeder.php');
        $stub = $stub = $this->files->get($path);
        $pos = strpos($stub, "{$uname}Seeder::class");
        if ($pos !== false) {
            return;
        }
        $value = '$this->call('.$uname."Seeder::class);\r\n\t\t//{{seeder}}";
        $stub = str_replace('//{{seeder}}', $value, $stub);

        $this->files->put($path, $stub);

    }

    protected function getSeederAttributeValue($type)
    {
        switch ($type) {
            case 'string':
                return "'Test 1'";
            case 'boolean':
                return 'true';
            case 'datetime':
            case 'timestamp':
                return "'".Carbon::now()->format('Y-m-d H:i:s')."'";
            case 'date':
                return "'".Carbon::now()->format('Y-m-d')."'";
            case 'time':
                return "'".Carbon::now()->format('H:i:s')."'";
            case 'integer':
            case 'tinyInteger':
            case 'unsignedBigInteger':
            case 'double':
                return 1;
            default:
                return "''";
        }
    }

    protected function makeModel()
    {
        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $stub = $stub = $this->files->get($this->getStub('model'));
        $fillable = [];
        $relations = [];
        $attrs = [];
        $replaces = [
            '{{namespace}}' => '',
            '{{entity_uname}}' => $uname,
            '{{entity_name}}' => $name,
        ];

        foreach ($this->entity['attributes'] as $attribute) {
            $fillable[] = "'".strtolower($attribute['code'])."'";
            if (isset($attribute['relation'])) {
                $relName = str_replace('_id', '', $attribute['code']);
                $fieldName = $attribute['relation']['main_attr'];
                $relation = '
    public function '.$relName.'()
    {
        return $this->belongsTo('.ucwords(Str::camel(Str::singular($attribute['relation']['table']))).'::class);
    }';

                $attr = '
    public function get'.ucwords(Str::camel($relName)).ucwords(Str::camel($fieldName)).'Attribute()
    {
        return ($this->'.$relName.') ? $this->'.$relName.'->'.$fieldName.' : null;
    }';

                $relations[] = $relation;
                $attrs[] = $attr;
            }
        }

        $replaces['{{fillable}}'] = implode(",\r\n\t\t", $fillable);
        $replaces['{{relations}}'] = implode("\r\n\t", $relations);
        $replaces['{{attributes}}'] = implode("\r\n\t", $attrs);

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }
        $path = $this->getPath("App\\Models\\".$uname);

        $this->makeDirectory($path);

        $this->files->put($path, $stub);

    }

    protected function makeRequest()
    {
        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $pluralTableName = Str::plural($name);
        $stub = $stub = $this->files->get($this->getStub('request'));
        $fields = [];
        $annotations = [];
        $replaces = [
            '{{namespace}}' => '',
            '{{entity_uname}}' => $uname,
            '{{entity_name}}' => $name,
        ];

        foreach ($this->entity['attributes'] as $attribute) {
            if ($attribute['code'] == 'updated_by_id' || $attribute['code'] == 'created_by_id') {
                continue;
            }
            $type = $this->getRequestFieldType($attribute['type']);
            if (isset($attribute['nullable']) && !$attribute['nullable']) {
                $type .= '|required';
            } elseif (isset($attribute['nullable']) && $attribute['nullable']) {
                $type .= '|nullable';
            }
            if (isset($attribute['unique']) && $attribute['unique']) {
                $type .= "|unique:{$pluralTableName},{$attribute['code']},' . \$this->{$name}?->id";
            }
            $exampleValue = str_replace("'", '', $this->getSeederAttributeValue($attribute['type']));
            $unique = (isset($attribute['unique']) && $attribute['unique']) ? "" : "'";
            $fields[] = "'".strtolower($attribute['code'])."' => '".$type.$unique;
            $annotations[] = "'".strtolower($attribute['code'])."' => [\r\n\t\t\t\t".
                "'description' => '".$attribute['name']."',\r\n\t\t\t\t".
                "'example' => '{$exampleValue}'\r\n\t\t\t".
                "]";
        }

        $replaces['{{fields}}'] = implode(",\r\n\t\t\t", $fields);
        $replaces['{{annotations}}'] = implode(",\r\n\t\t\t", $annotations);

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }
        $path = $this->getPath("App\\Http\\Requests\\".$uname.'Request');

        $this->makeDirectory($path);

        $this->files->put($path, $stub);

    }

    protected function makeCollection()
    {
        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $stub = $stub = $this->files->get($this->getStub('collection'));
        $replaces = [
            '{{namespace}}' => '',
            '{{entity_uname}}' => $uname,
            '{{entity_name}}' => $name,
        ];

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }
        $path = $this->getPath("App\\Http\\Resources\\".$uname.'Collection');

        $this->makeDirectory($path);

        $this->files->put($path, $stub);

    }

    protected function makeResource()
    {
        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $stub = $stub = $this->files->get($this->getStub('resource'));
        $fillable = [];
        $replaces = [
            '{{namespace}}' => '',
            '{{entity_uname}}' => $uname,
            '{{entity_name}}' => $name,
        ];

        foreach ($this->entity['attributes'] as $attribute) {
            $fillable[] = "'".strtolower($attribute['code']).'\' => $this->'.strtolower($attribute['code']);
            if (isset($attribute['relation'])) {
                $relName = str_replace('_id', '', $attribute['code']);
                $fillable[] = "'".strtolower($relName).'\' => $this->'.strtolower($relName);
            }
        }

        $replaces['{{fillable}}'] = implode(",\r\n\t\t\t", $fillable);

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }
        $path = $this->getPath("App\\Http\\Resources\\".$uname.'Resource');

        $this->makeDirectory($path);

        $this->files->put($path, $stub);

    }

    protected function makeController()
    {
        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $stub = $stub = $this->files->get($this->getStub('controller'));
        $replaces = [
            '{{namespace}}' => '',
            '{{entity_uname}}' => $uname,
            '{{entity_name}}' => $name,
        ];

        foreach ($replaces as $key => $value) {
            $stub = str_replace($key, $value, $stub);
        }
        $path = $this->getPath("App\\Http\\Controllers\\Api\\V1\\".$uname.'Controller');

        $this->makeDirectory($path);

        $this->files->put($path, $stub);
    }

    protected function addRoute()
    {
        $routes_path = config('crudgenerator.routes_path');
        $name = strtolower($this->entity['code']);
        $uname = ucwords(Str::camel($name));
        $path = base_path($routes_path);
        $stub = $stub = $this->files->get($path);
        $pos = strpos($stub, "'".str_replace('_', '-', Str::plural($name, 2))."'");
        if ($pos !== false) {
            return;
        }
        $value = "'".str_replace('_', '-',
                Str::plural($name, 2))."' => ".$uname."Controller::class,\r\n\t\t\t\t//{{resource}}";
        $stub = str_replace('//{{resource}}', $value, $stub);


        $this->makeDirectory($path);

        $this->files->put($path, $stub);
    }

    /**
     * Get the stub file for the generator.
     *
     * @param $name
     * @return string
     */
    protected function getStub($name)
    {
        $relativePath = '/stubs/'.$name.'.stub';

        return file_exists($customPath = $this->laravel->basePath(trim($relativePath, '/')))
            ? $customPath
            : __DIR__.$relativePath;
    }

    /**
     * Get the destination class path.
     *
     * @param  string  $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'].'/'.str_replace('\\', '/', $name).'.php';
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace();
    }

    protected function saveToEntitySeeder(): void
    {
        $path = base_path('database/seeders/EntitySeeder.php');
        $stub = $this->files->get($path);
        $pos = strpos($stub, "'".$this->entity['code']."'");
        if ($pos !== false) {
            return;
        }
        $value = "[
                'name'           => '{$this->entity['name']}',
                'code'           => '{$this->entity['code']}',
                'main_attribute' => '{$this->entity['main_attribute']}',
            ],
            //{{entity}}";
        $stub = str_replace('//{{entity}}', $value, $stub);


        $this->makeDirectory($path);

        $this->files->put($path, $stub);
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function saveToEntityAttributeSeeder()
    {
        $name = strtolower($this->entity['code']);
        $data = [];
        foreach ($this->entity['attributes'] as $attribute) {
            $nullable = (isset($attribute['nullable']) && $attribute['nullable'] == true) ? 'true' : 'false';
            $unique = (isset($attribute['unique']) && $attribute['unique'] == true) ? 'true' : 'false';
            $value = [
                'name' => $attribute['name'],
                'code' => $attribute['code'],
                'type' => $attribute['type'],
                'nullable' => filter_var($nullable, FILTER_VALIDATE_BOOLEAN),
                'unique' => filter_var($unique, FILTER_VALIDATE_BOOLEAN),
            ];
            $data[] = $value;
        }
        File::put(database_path("seeders/attributes/{$name}.php"), '<?php return '.var_export($data, true).';');

//        $path = database_path('database/seeders/EntityAttributeSeeder.php');
//        $stub = $this->files->get($path);
//
//        $match = null;
//        preg_match_all("/\[\X*\];/imsxu",$stub,$match);
//        dd($match);
//        $value = "'{$name}' => [\r\n\t\t\t";
//        foreach ($this->entity['attributes'] as $attribute) {
//            $nullable = (isset($attribute['nullable']) && $attribute['nullable'] == true) ? 'true' : 'false';
//            $unique   = (isset($attribute['unique']) && $attribute['unique'] == true) ? 'true' : 'false';
//            $value    .= " [
//                    'name'     => '{$attribute['name']}',
//                    'code'     => '{$attribute['code']}',
//                    'type'     => '{$attribute['type']}',
//                    'nullable' => {$nullable},
//                    'unique'   => {$unique},
//                ],\r\n\t\t\t";
//        }
//        $value .= "],
//            //{{attributes}}
//            ";
//        $stub  = str_replace('//{{attributes}}', $value, $stub);
//
//
//        $this->makeDirectory($path);
//
//        $this->files->put($path, $stub);

        return true;
    }

    /**
     * @param $attribute
     * @return string
     */
    protected function getRequestFieldType($type): string
    {
        switch ($type) {
            case 'tinyInteger':
            case 'unsignedBigInteger':
            case 'double':
                return 'integer';
            case 'text':
                return 'string';
            default:
                return $type;
        }
    }

    protected function file_build_path(...$segments)
    {
        return join(DIRECTORY_SEPARATOR, $segments);
    }
}
