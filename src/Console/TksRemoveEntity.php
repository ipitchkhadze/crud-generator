<?php

namespace Tks\CrudGenerator\Console;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Tks\CrudGenerator\Models\Entity;

class TksRemoveEntity extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tks:remove-entity';

    protected $files;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove entity';

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entityName = $this->ask('Entity to remove');
        $entity = Entity::where('code', $entityName)->orWhere('name', $entityName)->first();
        $uname = ucwords($entity->code);
        $entity->attributes()->delete();
        $entity->delete();

        $toRemove = [
            'controller' => $this->getPath("App\\Http\\Controllers\\Api\\V1\\" . $uname . 'Controller'),
            'model'      => $this->getPath("App\\Models\\" . $uname),
            'resource'   => $this->getPath("App\\Http\\Resources\\" . $uname . 'Resource'),
            'collection' => $this->getPath("App\\Http\\Resources\\" . $uname . 'Collection'),
            'request'    => $this->getPath("App\\Http\\Requests\\" . $uname . 'Request'),

        ];
        foreach ($toRemove as $file) {
            $this->files->delete($file);
        }
        $this->info('Entity removed successfully!');
    }

    /**
     * Get the stub file for the generator.
     *
     * @param $name
     * @return string
     */
    protected function getStub($name)
    {
        $relativePath = '/stubs/' . $name . '.stub';

        return file_exists($customPath = $this->laravel->basePath(trim($relativePath, '/')))
            ? $customPath
            : __DIR__ . $relativePath;
    }

    /**
     * Get the destination class path.
     *
     * @param string $name
     * @return string
     */
    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->rootNamespace(), '', $name);

        return $this->laravel['path'] . '/' . str_replace('\\', '/', $name) . '.php';
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        return $path;
    }

    /**
     * Get the root namespace for the class.
     *
     * @return string
     */
    protected function rootNamespace()
    {
        return $this->laravel->getNamespace();
    }
}
