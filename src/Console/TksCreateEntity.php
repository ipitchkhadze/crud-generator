<?php

namespace Tks\CrudGenerator\Console;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Tks\CrudGenerator\Services\CrudGeneratorService;

class TksCreateEntity extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tks:create-entity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create entity CRUD';

    public function __construct(Filesystem $files)
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $path = $this->file_build_path(__DIR__, "entities", "acc_payments.json");
        $crudGeneratorService = new CrudGeneratorService();
        $crudGeneratorService->generate($path);
    }
}
