<?php

namespace Tks\CrudGenerator\Models;

use Illuminate\Database\Eloquent\Model;

class EntityAttribute extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'code','type','nullable','unique'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [

    ];

    public function attributes(){

    }

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }
}
