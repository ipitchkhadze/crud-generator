<?php

namespace Tks\CrudGenerator;

use Illuminate\Support\ServiceProvider;
use Tks\CrudGenerator\Console\TksCreateEntity;
use Tks\CrudGenerator\Console\TksRemoveEntity;

class CrudGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'tks');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'tks');
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        // Publishing is only necessary when using the CLI.
        if ($this->app->runningInConsole()) {
            $this->bootForConsole();
        }
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/crudgenerator.php', 'crudgenerator');

        // Register the service the package provides.
        $this->app->singleton('crudgenerator', function ($app) {
            return new CrudGenerator;
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['crudgenerator'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/crudgenerator.php' => config_path('crudgenerator.php'),
        ], 'crudgenerator.config');

        // Publishing the views.
        /*$this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/tks'),
        ], 'crudgenerator.views');*/

        // Publishing assets.
        /*$this->publishes([
            __DIR__.'/../resources/assets' => public_path('vendor/tks'),
        ], 'crudgenerator.views');*/

        // Publishing the translation files.
        /*$this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/tks'),
        ], 'crudgenerator.views');*/

        //Registering package commands.
        $this->commands([
            TksCreateEntity::class,
            TksRemoveEntity::class
        ]);
    }
}
